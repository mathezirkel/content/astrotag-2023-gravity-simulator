from typing import Tuple, List

from matplotlib import pyplot as plt


def draw_circle(x: float, y: float) -> None:
    plt.grid(visible=True)
    plt.xlim(-100, 100)
    plt.ylim(-100, 100)
    circle = plt.Circle((x, y), radius=2, fill=True)
    plt.pause(0.001)
    plt.clf()
    plt.gcf().gca().add_artist(circle)


def draw_orbit(x: List[float], y: List[float]) -> None:
    plt.grid(visible=True)
    plt.xlim(-100, 100)
    plt.ylim(-100, 100)
    plt.plot(x, y)
    plt.pause(0.001)


def tick(x: float, y: float, vx: float, vy: float, dt: float, m: float) -> Tuple[float, float, float, float]:
    xp = x + vx * dt
    yp = y + vy * dt
    vxp = vx - m * x / (x**2 + y**2)**(3 / 2) * dt
    vyp = vy - m * y / (x**2 + y**2)**(3 / 2) * dt
    return xp, yp, vxp, vyp


def animate_movement(x: float, y: float, vx: float, vy: float, dt: float, m: float) -> None:
    while True:
        # Update position in every time step
        x, y, vx, vy = tick(x, y, vx, vy, dt, m)

        # Plot the position
        draw_circle(x, y)


def animate_orbit(x: float, y: float, vx: float, vy: float, dt: float, m: float) -> None:
    xt = [x]
    yt = [y]
    vxt = [vx]
    vyt = [vy]

    while True:
        xp, yp, vxp, vyp = tick(xt[-1], yt[-1], vxt[-1], vyt[-1], dt, m)
        xt.append(xp)
        yt.append(yp)
        vxt.append(vxp)
        vyt.append(vyp)

        draw_orbit(xt, yt)


def main():
    x: float = 50
    y: float = 0
    vx: float = 0
    vy: float = 1

    dt: float = 1
    m: float = 50

    # animate_movement(x, y, vx, vy, dt, m)
    animate_orbit(x, y, vx, vy, dt, m)


if __name__ == "__main__":
    main()
